#[cfg(not(feature = "std"))]
use alloc::{
    collections::btree_map::BTreeMap,
    format,
    string::{String, ToString},
};
use an_operation_support::describe;
use cid::Cid;
#[cfg(feature = "std")]
use std::collections::BTreeMap;

/// RAW codec from the multiformats
const RAW: u64 = 0x55;

/// op_cid
///
/// Compute the multibase encoded Content IDentifier of the
/// [`op_multihash::U64MultihashWrapper`] provided in input.
///
/// The `describe` attribute allow generation of manifest
/// with the following explicit properties:
///
///  * _group_: SYS
///  * _config_: no configuration parameters
///  * _features_: support std lib as well as nostd
///
/// # Arguments
///  - hash: [`op_multihash::U64MultihashWrapper`]
///
/// # Return
/// A [`Result`] having an Ok value of [`String`] type and a Err value of [`String`] type.
///
/// **NOTE:** The function returns asynchronously, by mean of a `Future`.
#[describe([
  groups = [
    "SYS",
  ],
  config = [],
  features = [
    "std"
  ]
])]
pub async fn execute(
    hash: &op_multihash::U64MultihashWrapper,
    _: BTreeMap<String, String>,
) -> Result<String, String> {
    let hash = cid::multihash::MultihashGeneric::wrap(hash.get_code(), hash.get_digest())
        .map_err(|error| error.to_string())?;
    let cid = Cid::new_v1(RAW, hash);
    let multibase = multibase::encode(multibase::Base::Base32Lower, &cid.to_bytes());
    Ok(multibase)
}

#[cfg(test)]
mod tests {
    use crate::execute;
    use op_multihash::U64MultihashWrapper;
    use serde_json;
    use std::collections::BTreeMap;

    #[test]
    fn test_describe() {
        use an_operation_support::operation::OperationManifestData;
        use serde_json;

        let manifest_data: OperationManifestData =
            serde_json::from_str(&crate::describe()).unwrap();

        assert_eq!("op_cid", manifest_data.name);
        assert_eq!(1, manifest_data.inputs.len());
        assert_eq!(
            "op_multihash::U64MultihashWrapper",
            manifest_data.inputs.get(0).unwrap()
        );
        assert_eq!("String", manifest_data.output);
        assert_eq!("std", manifest_data.features.get(0).unwrap());
    }

    #[tokio::test]
    async fn test_execute() {
        let input = U64MultihashWrapper::default();
        let op_output = execute(&input, BTreeMap::new()).await.unwrap();
        assert_eq!("bafkqaaa", op_output);
    }
}
